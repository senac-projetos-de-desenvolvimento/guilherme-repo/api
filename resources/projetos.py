from flask import Blueprint, jsonify, request
from banco import db
from flask_cors import CORS, cross_origin
from models.modelProjeto import Projeto
from models.modelImagem import Imagem
from flask_jwt_extended import jwt_required

projetos = Blueprint('projetos', __name__)


@projetos.route('/projetos')
@cross_origin()
def listagemProjetos():
    projetos = Projeto.query.order_by(Projeto.projeto).all()
    return jsonify([projeto.to_json() for projeto in projetos])

@projetos.route('/projetos/cliente/<int:clienteId>')
@cross_origin()
def listagemProjetosCliente(clienteId):
    projetos = Projeto.query.filter(Projeto.clienteId == clienteId).all()

    return jsonify([projeto.to_json() for projeto in projetos])    
    

@projetos.route('/projetos', methods=['POST'])
# Quando colocar o login descomentar essa linha
# @jwt_required
@cross_origin()
def cadastroProjetos():
    projeto = Projeto.from_json(request.json)
    db.session.add(projeto)
    db.session.commit()
    return jsonify(projeto.to_json()), 201


@projetos.route('/projetos/<int:projetoId>', methods=['PUT'])
# Quando colocar o login descomentar essa linha
# @jwt_required
@cross_origin()
def alteracaoProjeto(projetoId):
    projeto = Projeto.query.get_or_404(projetoId)
    projeto.projeto = request.json['projeto']
    projeto.descricao = request.json['descricao']
    projeto.valor = request.json['valor']
    projeto.clienteId = request.json['clienteId']   
    db.session.add(projeto)
    db.session.commit()
    return jsonify(projeto.to_json()), 200   
    

@projetos.route('/projetos/<int:projetoId>')
@cross_origin()
def getByIdProjetos(projetoId):
    projetos = Projeto.query.order_by(Projeto.projetoId).filter(Projeto.projetoId.like(f'%{projetoId}%')).all()
    if len(projetos):
        return jsonify([projeto.to_json() for projeto in projetos]) 
    else:
        return jsonify({'id': 0, 'message': 'Nenhum projeto encontrado'}), 201  

@projetos.route('/projetos/imagem/<int:clienteId>')
@cross_origin()
def listageProjetosEspecificos(clienteId):
    projetos = Projeto.query.filter(Projeto.clienteId == clienteId).order_by(Projeto.projeto)

    for projeto in projetos:
        imagem = Imagem.query.filter(Imagem.registroId == projeto.projetoId and Imagem.tipoRegistro == 2).limit(1)
        print(imagem[0].urlImagem)
        projeto.imagem = imagem[0].urlImagem
        projeto.imagemId = imagem[0].imagemId
        # i=i+1
    return jsonify([projeto.to_json() for projeto in projetos])

@projetos.route('/projetos/pesq/<projeto>')
# Quando colocar o login descomentar essa linha
# @jwt_required
@cross_origin()
def pesquisaCliente(projeto):
    projetos = Projeto.query.order_by(Projeto.projeto).filter(Projeto.projeto.like(f'%{projeto}%')).all()
    if len(projetos):
        return jsonify([projeto.to_json() for projeto in projetos]) 
    else:
        return jsonify({'id': 0, 'message': 'Nenhum projeto encontrado'}), 400 


# @viagens.route('/viagens/estatisticas')
# def maior():
#     totalViagens = Viagem.query.count()
#     m = Viagem.query.order_by(Viagem.valor.desc()).limit(1).all()
#     mn = Viagem.query.order_by(Viagem.valor.asc()).limit(1).all()
#     maior = {
#         'id': m[0].idViagens,
#         'valor': m[0].valor,
#         'tipoViagem': m[0].tipoViagem
#         }
#     menor = {
#         'id': mn[0].idViagens,
#         'valor': mn[0].valor,
#         'tipoViagem': mn[0].tipoViagem
#         }
#     lista = []
#     lista.append({'totalViagens': totalViagens, 'maior': maior, 'menor': menor})
    
#     return jsonify(lista), 201



@projetos.route('/projetos/<int:projetoId>', methods=['DELETE'])
# Quando colocar o login descomentar essa linha
# @jwt_required
@cross_origin()
def exclui(projetoId):
    projetos = Projeto.query.filter_by(projetoId=projetoId).delete()
    if projetos:
        # Excursao.query.filter_by(usuarioId=usuarioId).delete()
        db.session.commit()
        return jsonify({'id': projetoId, 'message': 'Projeto excluído com sucesso'}), 200
    else:
      return jsonify({'id': 0, 'message': 'Projeto Não encontrado'}), 404      

@projetos.errorhandler(404)
def id_invalido(error):
    return jsonify({'id': 0, 'message': 'Projeto Não encontrado'}), 404  