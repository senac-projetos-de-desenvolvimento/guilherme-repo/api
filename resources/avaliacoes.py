from flask import Blueprint, jsonify, request, current_app,url_for
from banco import db
from flask_cors import CORS, cross_origin
from models.modelAvaliacao import Avaliacao
from flask_jwt_extended import jwt_required

# comentarios = Blueprint('comentarios', __name__)


avaliacoes = Blueprint('avaliacoes', __name__)

@avaliacoes.route('/avaliacoes/<int:limit>')
@cross_origin()
def listagemAvaliacoesTotais(limit):
    avaliacoes = Avaliacao.query.order_by(Avaliacao.avaliacaoId).limit(limit*2)
    return jsonify([avaliacao.to_json() for avaliacao in avaliacoes])

# @avaliacoes.route('/avaliacoes')
# @cross_origin()
# def listagemAvaliacoesNormal():
#     # avaliacoes = Avaliacao.query.order_by(Avaliacao.avaliacaoId).all()
#     page = request.args.get('page', 1, type=int)
#     pagination = Avaliacao.query.paginate(page, per_page=5,
#     error_out=False)
#     avaliacoes = pagination.items
#     prev = None
#     if pagination.has_prev:
#         prev = url_for('avaliacoes.listagemAvaliacoesNormal', page=page-1)
#     next = None
#     if pagination.has_next:
#         next = url_for('avaliacoes.listagemAvaliacoesNormal', page=page+1)
#     return jsonify({
#         'avaliacoes':[avaliacao.to_json() for avaliacao in avaliacoes],
#         'prev_url': prev,
#         'next_url': next,
#         'count': pagination.total
#     })

@avaliacoes.route('/avaliacoes/cliente/<int:clienteId>')
@cross_origin()
def listagemAvaliacoes(clienteId):
    avaliacoes = Avaliacao.query.filter(Avaliacao.clienteId == clienteId).all()

    return jsonify([avaliacao.to_json() for avaliacao in avaliacoes])

@avaliacoes.route('/avaliacoes/media/<int:clienteId>')
@cross_origin()
def listageMedias(clienteId):
    lista = []
    avaliacoes = Avaliacao.query.filter(Avaliacao.clienteId==clienteId).all()
    if len(avaliacoes) > 0:
        c = len(avaliacoes)
        countMediaAvaliacao = float(c)
        mediaOrganizacao = db.session.query(Avaliacao.avaliacaoId,db.func.sum(Avaliacao.organizacao)).group_by(Avaliacao.clienteId== clienteId).all()
        mediaAcabamento = db.session.query(Avaliacao.avaliacaoId,db.func.sum(Avaliacao.acabamento)).group_by(Avaliacao.clienteId== clienteId).all()
        mediaOrcamento = db.session.query(Avaliacao.avaliacaoId,db.func.sum(Avaliacao.orcamento)).group_by(Avaliacao.clienteId== clienteId).all()
        mediaTempoEntrega = db.session.query(Avaliacao.avaliacaoId,db.func.sum(Avaliacao.tempoEntrega)).group_by(Avaliacao.clienteId== clienteId).all()
        # countAvaliacao = mediaAvaliacoes.count()
        # for media in mediaAvaliacoes:
        for media in mediaAcabamento:
            a = media[1]/countMediaAvaliacao


        for media1 in mediaOrganizacao:
            o = media1[1]/countMediaAvaliacao

        for media2 in mediaOrcamento:
            og = media2[1]/countMediaAvaliacao

        for media3 in mediaTempoEntrega:
            t = media3[1]/countMediaAvaliacao

        for cliente in avaliacoes:
             cli = cliente.clienteId

        lista.append({
        'clienteId': cli,
        'organizacao': o,
        'acabamento':a,
        'orcamento':og,
        'tempoEntrega':t
        })

    
        return jsonify(lista),201
    else:
        return jsonify({'message': 'Ainda não temos nenhuma avaliação para esse cliente'})

@avaliacoes.route('/avaliacoes', methods=['POST'])
# @jwt_required
@cross_origin()
def cadastroAvaliacoes():
    avaliacao = Avaliacao.from_json(request.json)
    db.session.add(avaliacao)
    db.session.commit()
    return jsonify(avaliacao.to_json()), 201


@avaliacoes.route('/avaliacoes/<int:avaliacaoId>', methods=['PUT'])
@cross_origin()
def alteracaoAvaliacoes(avaliacaoId):
    avaliacao = Avaliacao.query.get_or_404(avaliacaoId)
    avaliacao.orcamento = request.json['orcamento']
    avaliacao.acabamento = request.json['acabamento']
    avaliacao.tempoEntrega = request.json['tempoEntrega']  
    avaliacao.organizacao = request.json['organizacao']  
    avaliacao.dataAvaliacao = request.json['dataAvaliacao']  
    avaliacao.horaAvaliacao = request.json['horaAvaliacao']  
    avaliacao.clienteId = request.json['clienteId']  
    avaliacao.usuarioId = request.json['usuarioId']  
    db.session.add(avaliacao)
    db.session.commit()
    return jsonify(avaliacao.to_json()), 201


@avaliacoes.route('/avaliacoes/<int:avaliacaoId>')
@cross_origin()
def getByIdAvaliacao(avaliacaoId):
    avaliacao = Avaliacao.query.get_or_404(avaliacaoId)
    return jsonify(avaliacao.to_json()), 200 


@avaliacoes.route('/avaliacoes/<int:avaliacaoId>', methods=['DELETE'])
@cross_origin()
def exclui(avaliacaoId):
    Avaliacao.query.filter_by(avaliacaoId=avaliacaoId).delete()
    db.session.commit()
    return jsonify({'id': avaliacaoId, 'message': 'Avaliacao excluída com sucesso'}), 200

@avaliacoes.errorhandler(404)
def id_invalido(error):
    return jsonify({'id': 0, 'message': 'Avaliacao não encontrada'}), 404 
