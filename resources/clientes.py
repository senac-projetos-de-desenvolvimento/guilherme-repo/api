from flask import Blueprint, jsonify, request
from banco import db
from flask_cors import CORS, cross_origin
from models.modelCliente import Cliente
from models.modelProjeto import Projeto
from models.modelAcesso import Acesso
from models.modelServico import Servico
from flask_jwt_extended import jwt_required
import smtplib
import os; import locale;  os.environ["PYTHONIOENCODING"] = "utf-8";

clientes = Blueprint('clientes', __name__)


@clientes.route('/clientes')
@cross_origin()
def listagemUsuarios():
    clientes = Cliente.query.order_by(Cliente.nome).all()
    return jsonify([cliente.to_json() for cliente in clientes])

@clientes.route('/acessos_qtd')
def acessosGraf():
    # clientes = db.session.query(Cliente).filter(Cliente.clienteId == 4).all()
    acessos = db.session.query(Acesso).filter(Acesso.tipoRegistro==1).group_by(Acesso.registroId).all()
    num =0
    lista = []
    for acesso in acessos:
        clientes = db.session.query(Cliente).filter(Cliente.clienteId == 4).all()
        # print(acesso[0].qtdAcesso)
        lista.append({'qtdAcesso': acessos[num].qtdAcesso, 'nome': clientes[num].nome})
        num=num +1

    print(lista)    
    return jsonify(lista), 201

@clientes.route('/clientes/projetos/inner')
@cross_origin()
def listagemProjetosClientesInner():
    # clientes = Cliente()
    # clientes = Cliente()


    clientes = Cliente.query.join(Projeto, Projeto.clienteId==Cliente.clienteId)\
        .filter(Projeto.clienteId == Cliente.clienteId).all()
    # print(clientes.get_json()[0])

    return jsonify({
        'clientes': [cliente.to_json() for cliente in clientes],
        # 'clientes_inner': clientes.get_json(),
        # 'projetos': valor,
        # 'clienteId': ClienteId
    })

# @clientes.route('/tipoProfissional')
# @cross_origin()
# def listagemClientesProfissional():
#     clientes = Cliente.query.filter(Cliente.tipoCliente == 1).all()
#     return jsonify([cliente.to_json() for cliente in clientes])

@clientes.route('/tipoProfissional')
@cross_origin()
def listagemClientesProfissional():
    clientes = db.session.query(Cliente).filter(Cliente.tipoCliente == 1).limit(5)
    num  = 0
    lista = []
    for cliente in clientes:
        acessos = db.session.query(Acesso).filter(Acesso.tipoRegistro == 0 and Acesso.registroId == clientes[num].clienteId).all()
        servicos = db.session.query(Servico).filter(Servico.clienteId == clientes[num].clienteId).all()

        lista.append({'bairro': clientes[num].bairro,'cep': clientes[num].cep,'cidade': clientes[num].cidade, \
            'clienteId': clientes[num].clienteId, 'cpfCnpj': clientes[num].cpfCnpj, 'email': clientes[num].email, \
            'logradouro': clientes[num].logradouro, 'nome': clientes[num].nome, 'numero': clientes[num].numero, \
            'telefone': clientes[num].telefone, 'tipoCliente': clientes[num].tipoCliente, 'urlProfissional': clientes[num].urlProfissional, \
            'qtdAcesso': acessos[num].qtdAcesso, 'acessoId': acessos[num].acessoId, 'servico': (servicos[0].clienteId if servicos else '0')})
        num=num +1
    return jsonify(lista), 201


# @clientes.route('/tipoLoja')
# @cross_origin()
# def listagemClientesLoja():
#     clientes = Cliente.query.filter(Cliente.tipoCliente == 2).all()
#     return jsonify([cliente.to_json() for cliente in clientes])

@clientes.route('/tipoLoja')
@cross_origin()
def listagemClientesLoja():
    clientes = db.session.query(Cliente).filter(Cliente.tipoCliente == 2).paginate(page, per_page=5,
    error_out=False)
    num  = 0
    lista = []
    for cliente in clientes:
        # print(f'clientes[num].clienteId {clientes[num].clienteId}')
        
        acessos = db.session.query(Acesso).filter(Acesso.tipoRegistro == 1 and Acesso.registroId == clientes[num].clienteId).all()
        print(f'acessos[0].acessoId {acessos[num].acessoId}')
        lista.append({'bairro': clientes[num].bairro,'cep': clientes[num].cep,'cidade': clientes[num].cidade, \
            'clienteId': clientes[num].clienteId, 'cpfCnpj': clientes[num].cpfCnpj, 'email': clientes[num].email, \
            'logradouro': clientes[num].logradouro, 'nome': clientes[num].nome, 'numero': clientes[num].numero, \
            'telefone': clientes[num].telefone, 'tipoCliente': clientes[num].tipoCliente, 'urlProfissional': clientes[num].urlProfissional, \
            'qtdAcesso': acessos[num].qtdAcesso, 'acessoId': acessos[num].acessoId})
        num=num +1
    avaliacoes = clientes.items
        # print(lista)    
    return jsonify(lista), 201
    

@clientes.route('/clientes', methods=['POST'])
# Quando colocar o login descomentar essa linha
# @jwt_required
@cross_origin()
def cadastroClientes():
    cliente = Cliente.from_json(request.json)


    server = smtplib.SMTP('smtp.gmail.com', 587)
    server.starttls()
    server.login('construobre2021@gmail.com', 'ObreNoite2021!')
    server.set_debuglevel(1)
    email = request.json['email']
    nome = request.json['nome']
    cpfCnpj = request.json['cpfCnpj']  
    cep = request.json['cep']  
    logradouro = request.json['logradouro']  
    cidade = request.json['cidade']  
    bairro = request.json['bairro']  
    numero = request.json['numero']  
    telefone = request.json['telefone']  
    senha = request.json['senha']  
    tipoCliente = request.json['tipoCliente']  
    urlProfissional = request.json['urlProfissional']
    msg = f'Seja bem vindo! senhor(a), para logar na plataforma utilize o usuario = {email} e senha = {senha}.'
    server.sendmail('construobre2021@gmail.com', email, msg)
    server.quit()

    db.session.add(cliente)
    db.session.commit()
    return jsonify(cliente.to_json()), 201


@clientes.route('/clientes/<int:clienteId>', methods=['PUT'])
# Quando colocar o login descomentar essa linha
# @jwt_required
@cross_origin()
def alteracaoCliente(clienteId):
    cliente = Cliente.query.get_or_404(clienteId)
    cliente.email = request.json['email']
    cliente.nome = request.json['nome']
    cliente.cpfCnpj = request.json['cpfCnpj']  
    cliente.cep = request.json['cep']  
    cliente.logradouro = request.json['logradouro']  
    cliente.cidade = request.json['cidade']  
    cliente.bairro = request.json['bairro']  
    cliente.numero = request.json['numero']  
    cliente.telefone = request.json['telefone']  
    cliente.senha = request.json['senha']  
    cliente.tipoCliente = request.json['tipoCliente']  
    cliente.urlProfissional = request.json['urlProfissional']  
    db.session.add(cliente)
    db.session.commit()
    return jsonify(cliente.to_json()), 200   
    

# @clientes.route('/clientes/<int:clienteId>')
# @cross_origin()
# def getByIdClientes(clienteId):
#     clientes = Cliente.query.order_by(Cliente.clienteId).filter(Cliente.clienteId.like(f'%{clienteId}%')).all()
#     if len(clientes):
#         return jsonify([cliente.to_json() for cliente in clientes]) 
#     else:
#         return jsonify({'id': 0, 'message': 'Nenhum cliente encontrado'}), 201  

@clientes.route('/clientes/<int:clienteId>')
@cross_origin()
def getByIdClientes(clienteId):
    clientes = db.session.query(Cliente).filter(Cliente.clienteId == clienteId)
    if clientes.count() > 0:
        num  = 0
        lista = []

        acessos = Acesso.query.filter(Acesso.registroId == clienteId and Acesso.tipoRegistro == (clientes[num].tipoCliente -1 )).all()
        servicos = db.session.query(Servico).filter(Servico.clienteId == clientes[num].clienteId).all()
        lista.append({'bairro': clientes[num].bairro,'cep': clientes[num].cep,'cidade': clientes[num].cidade, \
            'clienteId': clientes[num].clienteId, 'cpfCnpj': clientes[num].cpfCnpj, 'email': clientes[num].email, \
            'logradouro': clientes[num].logradouro, 'nome': clientes[num].nome, 'numero': clientes[num].numero, \
            'telefone': clientes[num].telefone, 'tipoCliente': clientes[num].tipoCliente, 'urlProfissional': clientes[num].urlProfissional, \
            'qtdAcesso': acessos[num].qtdAcesso, 'acessoId': acessos[num].acessoId, 'servico': (servicos[0].clienteId if servicos else '0')})  
        return jsonify(lista), 201  
    else:
        return jsonify({'id': 0}), 201


@clientes.route('/clientes/pesq/<nome>')
# Quando colocar o login descomentar essa linha
# @jwt_required
@cross_origin()
def pesquisaCliente(nome):
    clientes = Cliente.query.order_by(Cliente.nome).filter(Cliente.nome.like(f'%{nome}%')).all()
    if len(clientes):
        return jsonify([cliente.to_json() for cliente in clientes]) 
    else:
        return jsonify({'id': 0, 'message': 'Nenhum usuario encontrado'}), 400

@clientes.route('/clientes/profissionais/<profissional>')
# Quando colocar o login descomentar essa linha
# @jwt_required
@cross_origin()
def pesquisaProfissional(profissional):
    # clientes = Cliente.query.order_by(Cliente.nome).filter(Cliente.nome.like(f'%{profissional}%').filter(Cliente.query.filter(Cliente.tipoCliente == 1)).all()
    # clientes = Cliente.query.filter(Cliente.tipoCliente == 1).filter(Cliente.nome.like(f'%{profissional}%'))
    clientes = Cliente.query.filter(Cliente.tipoCliente == 1).filter(Cliente.nome.like(f'%{profissional}%')).order_by(Cliente.nome)
    print(clientes)
    if clientes.count() > 0 :
        return jsonify([cliente.to_json() for cliente in clientes]) 
    else:
        return jsonify({'id': 0, 'message': 'Nenhum profissional encontrado'}), 400

@clientes.route('/clientes/lojas/<loja>')
# Quando colocar o login descomentar essa linha
# @jwt_required
@cross_origin()
def pesquisaLoja(loja):
    # clientes = Cliente.query.order_by(Cliente.nome).filter(Cliente.nome.like(f'%{profissional}%').Cliente.query.filter(Cliente.tipoCliente == 1)).all()
    # clientes = Cliente.query.filter(Cliente.tipoCliente == 2).filter(Cliente.nome.like(f'%{loja}%'))
    clientes = Cliente.query.filter(Cliente.tipoCliente == 2).filter(Cliente.nome.like(f'%{loja}%')).order_by(Cliente.nome)
    print(clientes)
    if clientes.count() > 0 :
        return jsonify([cliente.to_json() for cliente in clientes]) 
    else:
        return jsonify({'id': 0, 'message': 'Nenhum loja encontrado'}), 400

# @clientes.route('/clientes/loja/<nome>')
# # Quando colocar o login descomentar essa linha
# # @jwt_required
# @cross_origin()
# def pesquisaLoja(nome):
#     clientes = Cliente.query.order_by(Cliente.nome).filter(Cliente.nome.like(f'%{nome}%')) and Cliente.query.filter(Cliente.tipoCliente == 2).all()
#     print(clientes)
#     if clientes :
#         return jsonify([cliente.to_json() for cliente in clientes]) 
#     else:
#         return jsonify({'id': 0, 'message': 'Nenhum profissional encontrado'}), 400 


# @viagens.route('/viagens/estatisticas')
# def maior():
#     totalViagens = Viagem.query.count()
#     m = Viagem.query.order_by(Viagem.valor.desc()).limit(1).all()
#     mn = Viagem.query.order_by(Viagem.valor.asc()).limit(1).all()
#     maior = {
#         'id': m[0].idViagens,
#         'valor': m[0].valor,
#         'tipoViagem': m[0].tipoViagem
#         }
#     menor = {
#         'id': mn[0].idViagens,
#         'valor': mn[0].valor,
#         'tipoViagem': mn[0].tipoViagem
#         }
#     lista = []
#     lista.append({'totalViagens': totalViagens, 'maior': maior, 'menor': menor})
    
#     return jsonify(lista), 201


@clientes.route('/clientes/<int:clienteId>', methods=['DELETE'])
# Quando colocar o login descomentar essa linha
# @jwt_required
@cross_origin()
def exclui(clienteId):
    clientes = Cliente.query.filter_by(clienteId=clienteId).delete()
    if clientes:
        # Excursao.query.filter_by(usuarioId=usuarioId).delete()
        db.session.commit()
        return jsonify({'id': clienteId, 'message': 'Cliente excluído com sucesso'}), 200
    else:
      return jsonify({'id': 0, 'message': 'Cliente Não encontrado'}), 404      

@clientes.errorhandler(404)
def id_invalido(error):
    return jsonify({'id': 0, 'message': 'Cliente Não encontrado'}), 404  