from flask import Blueprint, jsonify, request
from banco import db
from models.modelLogin import Login
from models.modelUsuario import Usuario
from config import config
import hashlib
# from flask_jwt_extended import create_access_token, jwt_required, get_raw_jwt
from blacklist import blacklist

logins = Blueprint('logins', __name__)


@logins.route('/logins')
def listagem():
    logins = Login.query.order_by(Login.nome).all()
    return jsonify([login.to_json() for login in logins])


@logins.route('/logins', methods=['POST'])
def inclusao():
    login = Login.from_json(request.json)
    db.session.add(login)
    db.session.commit()
    return jsonify(login.to_json()), 201


@logins.route('/login', methods=['POST'])
def login():
    if not request.is_json:
        return jsonify({"msg": "Missing JSON in request"}), 400

    email = request.json.get('email', None)
    senha = request.json.get('senha', None)
    if not email:
        return jsonify({"msg": "Missing email parameter"}), 400
    if not senha:
        return jsonify({"msg": "Missing senha parameter"}), 400

    senha += config.SALT
    senha_md5 = hashlib.md5(senha.encode()).hexdigest()

    login = Login.query \
        .filter(Login.email == email) \
        .filter(Login.senha == senha_md5) \
        .first()

    usuarioId = 0
    if login:

        if login.tipoPermissao == 4: 
            usuario = Usuario.query \
            .filter(Usuario.usuarioId == login.registroId) \
            .first()

            usuarioId = usuario.usuarioId
        else:
            usuarioId = 0

        # Identity can be any data that is json serializable
        access_token = create_access_token(identity=email)
        return jsonify({"user": login.nome, "access_token": access_token, "tipoPermissao": login.tipoPermissao, "registroId": login.registroId, "usuarioId": usuarioId}), 200
    else:
        return jsonify({"user": None, "access_token": None}), 200


@logins.route('/logout')
# @jwt_required
def logout():
    jti = get_raw_jwt()['jti']
    blacklist.add(jti)
    return jsonify({"msg": "Successfully logged out"}), 200
