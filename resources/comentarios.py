from flask import Blueprint, jsonify, request
from banco import db
from flask_cors import CORS, cross_origin
from models.modelComentario import Comentario
from models.modelCliente import Cliente
from flask_jwt_extended import jwt_required

# comentarios = Blueprint('comentarios', __name__)


comentarios = Blueprint('comentarios', __name__)


# @comentarios.route('/comentarios')
# @cross_origin()
# def listagemComentarios():
#     comentarios = Comentario.query.order_by(Comentario.comentarioPessoa).all()
#     return jsonify([comentario.to_json() for comentario in comentarios])
#     print([comentario.to_json()])

@comentarios.route('/comentarios/cliente/<int:clienteId>')
@cross_origin()
def listagemComentarios(clienteId):
    comentarios = Comentario.query.filter(Comentario.clienteId == clienteId).all()

    return jsonify([comentario.to_json() for comentario in comentarios])


@comentarios.route('/comentarios', methods=['POST'])
# @jwt_required
@cross_origin()
def cadastroComentarios():
    comentario = Comentario.from_json(request.json)
    db.session.add(comentario)
    db.session.commit()
    return jsonify(comentario.to_json()), 201


@comentarios.route('/comentarios/<int:comentarioId>', methods=['PUT'])
@cross_origin()
def alteracaoComentarios(comentarioId):
    comentario = Comentario.query.get_or_404(comentarioId)
    comentario.comentarioPessoa = request.json['comentarioPessoa']
    comentario.dataComentario = request.json['dataComentario']
    comentario.horaComentario = request.json['horaComentario']  
    comentario.clienteId = request.json['clienteId']  
    comentario.usuarioId = request.json['usuarioId']  
    db.session.add(comentario)
    db.session.commit()
    return jsonify(comentario.to_json()), 201


@comentarios.route('/comentarios/<int:comentarioId>')
@cross_origin()
def getByIdComentario(comentarioId):
    comentario = Comentario.query.get_or_404(comentarioId)
    return jsonify(comentario.to_json()), 200 


@comentarios.route('/comentarios/<int:comentarioId>', methods=['DELETE'])
@cross_origin()
def exclui(comentarioId):
    Comentario.query.filter_by(comentarioId=comentarioId).delete()
    db.session.commit()
    return jsonify({'id': comentarioId, 'message': 'Comentário excluído com sucesso'}), 200

@comentarios.errorhandler(404)
def id_invalido(error):
    return jsonify({'id': 0, 'message': 'Comentário não encontrado'}), 404 
