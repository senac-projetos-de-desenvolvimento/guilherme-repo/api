from flask import Blueprint, jsonify, request
from banco import db
from flask_cors import CORS, cross_origin
from models.modelComentario import Comentario
from models.modelServico import Servico
from flask_jwt_extended import jwt_required

# comentarios = Blueprint('comentarios', __name__)


servicos = Blueprint('servicos', __name__)


@servicos.route('/servicos')
@cross_origin()
def listagemServicos():
    servicos = Servico.query.order_by(Servico.clienteId).all()
    return jsonify([servico.to_json() for servico in servicos])
    # print([servico.to_json()])

@servicos.route('/servicos/servico/<int:servicoId>')
@cross_origin()
def listagemServico(servicoId):
    servicos = Servico.query.filter(Servico.servicoId == servicoId).all()

    return jsonify([servico.to_json() for servico in servicos])


@servicos.route('/servicos', methods=['POST'])
# @jwt_required
@cross_origin()
def cadastroServicos():
    servico = Servico.from_json(request.json)
    db.session.add(servico)
    db.session.commit()
    return jsonify(servico.to_json()), 201


@servicos.route('/servicos/<int:servicoId>', methods=['PUT'])
@cross_origin()
def alteracaoServicos(servicoId):
    servico = Comentario.query.get_or_404(servicoId)
    servico.dataContratacao = request.json['dataContratacao']
    servico.dataConclusao = request.json['dataConclusao']  
    servico.clienteId = request.json['clienteId']  
    servico.usuarioId = request.json['usuarioId']  
    db.session.add(servico)
    db.session.commit()
    return jsonify(servico.to_json()), 201


@servicos.route('/servicos/<int:servicoId>')
@cross_origin()
def getByIdServicos(servicoId):
    servicos = Servico.query.order_by(Servico.clienteId).filter(Servico.clienteId.like(f'%{servicoId}%')).all()
    if len(servicos):
        return jsonify([servico.to_json() for servico in servicos]) 
    else:
        return jsonify({'id': 0, 'message': 'Nenhum servico encontrado'}), 201


@servicos.route('/servicos/<int:servicoId>', methods=['DELETE'])
@cross_origin()
def exclui(servicoId):
    Servico.query.filter_by(servicoId=servicoId).delete()
    db.session.commit()
    return jsonify({'id': servicoId, 'message': 'Servico excluído com sucesso'}), 200

@servicos.errorhandler(404)
def id_invalido(error):
    return jsonify({'id': 0, 'message': 'Servico não encontrado'}), 404 
