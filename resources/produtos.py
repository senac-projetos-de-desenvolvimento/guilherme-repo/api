from flask import Blueprint, jsonify, request
from banco import db
from flask_cors import CORS, cross_origin
from models.modelProduto import Produto
from models.modelImagem import Imagem
from flask_jwt_extended import jwt_required

produtos = Blueprint('produtos', __name__)


@produtos.route('/produtos')
@cross_origin()
def listagemProdutos():
    produtos = Produto.query.order_by(Produto.produto).all()
    return jsonify([produto.to_json() for produto in produtos])



@produtos.route('/produtos/imagens')
@cross_origin()
def listagemProdutosImagens():
    produtos = Produto.query.order_by(Produto.produto).all()
    # i = 0
    for produto in produtos:
        imagem = Imagem.query.filter(Imagem.registroId == produto.produtoId and Imagem.tipoRegistro == 3).limit(1)
        produto.imagem = imagem[0].urlImagem
        produto.imagemId = imagem[0].imagemId
        # i=i+1
    return jsonify([produto.to_json() for produto in produtos])

@produtos.route('/produtos/cliente/<int:clienteId>')
@cross_origin()
def listagemProdutosEspecificos(clienteId):
    produtos = Produto.query.filter(Produto.clienteId == clienteId).order_by(Produto.produto)
    return jsonify([produto.to_json() for produto in produtos])
    

@produtos.route('/produtos/imagens/loja/<int:clienteId>')
@cross_origin()
def listagemProdutosImagensEsp(clienteId):
    produtos = Produto.query.order_by(Produto.produto).filter(Produto.clienteId == clienteId).all()
    # i = 0
    for produto in produtos:
        imagem = Imagem.query.filter(Imagem.registroId == produto.produtoId and Imagem.tipoRegistro == 3).limit(1)
        produto.imagem = imagem[0].urlImagem
        produto.imagemId = imagem[0].imagemId
        # i=i+1
    return jsonify([produto.to_json() for produto in produtos])

@produtos.route('/produtos', methods=['POST'])
# Quando colocar o login descomentar essa linha
# @jwt_required
@cross_origin()
def cadastroProdutos():
    produto = Produto.from_json(request.json)
    db.session.add(produto)
    db.session.commit()
    return jsonify(produto.to_json()), 201


@produtos.route('/produtos/<int:produtoId>', methods=['PUT'])
# Quando colocar o login descomentar essa linha
# @jwt_required
@cross_origin()
def alteracaoProduto(produtoId):
    produto = Produto.query.get_or_404(produtoId)
    produto.produto = request.json['produto']
    produto.descricao = request.json['descricao']
    produto.valor = request.json['valor']
    produto.clienteId = request.json['clienteId']   
    produto.imagem = request.json['imagem']   
    produto.imagemId = request.json['imagemId']   
    db.session.add(produto)
    db.session.commit()
    return jsonify(produto.to_json()), 200   
    

@produtos.route('/produtos/<int:produtoId>')
@cross_origin()
def getByIdProdutos(produtoId):
    produtos = Produto.query.order_by(Produto.produtoId).filter(Produto.produtoId.like(f'%{produtoId}%')).all()
    if len(produtos):
        return jsonify([produto.to_json() for produto in produtos]) 
    else:
        return jsonify({'id': 0, 'message': 'Nenhum produto encontrado'}), 201   


@produtos.route('/produtos/pesq/<produto>')
# Quando colocar o login descomentar essa linha
# @jwt_required
@cross_origin()
def pesquisaProdutos(produto):
    produtos = Produto.query.order_by(Produto.produto).filter(Produto.produto.like(f'%{produto}%')).all()
    if len(produtos):
        return jsonify([produto.to_json() for produto in produtos]) 
    else:
        return jsonify({'id': 0, 'message': 'Nenhum produto encontrado'}), 400 


# @viagens.route('/viagens/estatisticas')
# def maior():
#     totalViagens = Viagem.query.count()
#     m = Viagem.query.order_by(Viagem.valor.desc()).limit(1).all()
#     mn = Viagem.query.order_by(Viagem.valor.asc()).limit(1).all()
#     maior = {
#         'id': m[0].idViagens,
#         'valor': m[0].valor,
#         'tipoViagem': m[0].tipoViagem
#         }
#     menor = {
#         'id': mn[0].idViagens,
#         'valor': mn[0].valor,
#         'tipoViagem': mn[0].tipoViagem
#         }
#     lista = []
#     lista.append({'totalViagens': totalViagens, 'maior': maior, 'menor': menor})
    
#     return jsonify(lista), 201


@produtos.route('/produtos/<int:produtoId>', methods=['DELETE'])
# Quando colocar o login descomentar essa linha
# @jwt_required
@cross_origin()
def exclui(produtoId):
    produtos = Produto.query.filter_by(produtoId=produtoId).delete()
    if produtos:
        # Excursao.query.filter_by(usuarioId=usuarioId).delete()
        db.session.commit()
        return jsonify({'id': produtoId, 'message': 'Produto excluído com sucesso'}), 200
    else:
      return jsonify({'id': 0, 'message': 'Produto Não encontrado'}), 404      

@produtos.errorhandler(404)
def id_invalido(error):
    return jsonify({'id': 0, 'message': 'Produto Não encontrado'}), 404  