from banco import db
from datetime import datetime

class Projeto(db.Model):
    __tablename__ = 'projetos'
    projetoId = db.Column(db.Integer, autoincrement=True, primary_key=True)
    projeto = db.Column(db.String(100), nullable=False)
    descricao = db.Column(db.String(300), nullable=False)
    imagem = db.Column(db.String(300), nullable=True)
    valor = db.Column(db.Float, nullable=False)


    clienteId = db.Column(db.Integer, db.ForeignKey(
        'clientes.clienteId'), nullable=False)

    # cliente = db.relationship('Cliente')
    
    def to_json(self):
        json_projetos = {
            'projetoId': self.projetoId,
            'projeto': self.projeto,
            'valor': self.valor,
            'descricao': self.descricao,
            'clienteId': self.clienteId,
            'imagem': self.imagem
        }
        return json_projetos

    @staticmethod
    def from_json(json_projetos):
        projeto = json_projetos.get('projeto')
        valor = json_projetos.get('valor')
        clienteId = json_projetos.get('clienteId')
        descricao = json_projetos.get('descricao')
        imagem = json_projetos.get('imagem')
        return Projeto(projeto=projeto, valor=valor, clienteId=clienteId, imagem=imagem, descricao=descricao)