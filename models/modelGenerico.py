from banco import db
from datetime import datetime

class Generico(db.Model):
    __tablename__ = 'genericos'
    id = db.Column(db.Integer, autoincrement=True, primary_key=True)
    orcamento = db.Column(db.Float(), nullable=True)
    acabamento = db.Column(db.Float(), nullable=True)
    tempoEntrega = db.Column(db.Float(), nullable=True)
    organizacao = db.Column(db.Float(), nullable=True)
    dataAvaliacao = db.Column(db.String(100), nullable=True)
    horaAvaliacao = db.Column(db.String(100), nullable=True)

    clienteId = db.Column(db.Integer, db.ForeignKey(
        'clientes.clienteId'), nullable=False)

    usuarioId = db.Column(db.Integer, db.ForeignKey(
        'usuarios.usuarioId'), nullable=False)

    def to_json(self):
        json_avaliacoes = {
            'avaliacaoId': self.avaliacaoId,
            'orcamento': self.orcamento,
            'acabamento': self.acabamento,
            'tempoEntrega': self.tempoEntrega,
            'organizacao': self.organizacao,
            'dataAvaliacao': self.dataAvaliacao,
            'horaAvaliacao': self.horaAvaliacao,
            'clienteId': self.clienteId,
            'usuarioId': self.usuarioId
        }
        return json_avaliacoes

    @staticmethod
    def from_json(json_avaliacoes):
        orcamento = json_avaliacoes.get('orcamento')
        acabamento = json_avaliacoes.get('acabamento')
        tempoEntrega = json_avaliacoes.get('tempoEntrega')
        organizacao = json_avaliacoes.get('organizacao')
        dataAvaliacao = json_avaliacoes.get('dataAvaliacao')
        horaAvaliacao = json_avaliacoes.get('horaAvaliacao')
        clienteId = json_avaliacoes.get('clienteId')
        usuarioId = json_avaliacoes.get('usuarioId')
        return Avaliacao(orcamento=orcamento, acabamento=acabamento, tempoEntrega=tempoEntrega, organizacao=organizacao, dataAvaliacao=dataAvaliacao, horaAvaliacao=horaAvaliacao, clienteId=clienteId, usuarioId=usuarioId)