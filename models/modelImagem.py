from banco import db
from datetime import datetime

class Imagem(db.Model):
    __tablename__ = 'imagens'
    imagemId = db.Column(db.Integer, autoincrement=True, primary_key=True)
    tipoRegistro = db.Column(db.Integer, nullable=False)
    registroId = db.Column(db.Integer,  nullable=False)
    urlImagem = db.Column(db.String(300), nullable=False)
    

    def to_json(self):
        json_imagens = {
            'imagemId': self.imagemId,
            'tipoRegistro': self.tipoRegistro,
            'registroId': self.registroId,
            'urlImagem': self.urlImagem
        }
        return json_imagens

    @staticmethod
    def from_json(json_imagens):
        tipoRegistro = json_imagens.get('tipoRegistro')
        registroId = json_imagens.get('registroId')
        urlImagem = json_imagens.get('urlImagem')
        return Imagem(tipoRegistro=tipoRegistro, registroId=registroId, urlImagem=urlImagem)