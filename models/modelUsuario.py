
from banco import db
import hashlib
from config import config
from datetime import datetime

class Usuario(db.Model):
  __tablename__ = 'usuarios'
  usuarioId = db.Column(db.Integer, autoincrement=True, primary_key=True)
  nome = db.Column(db.String(100), nullable=False)
  email = db.Column(db.String(100), unique=True, nullable=False)
  cpfCnpj = db.Column(db.String(14), nullable=False)
  cep = db.Column(db.String(8), nullable=False)
  logradouro = db.Column(db.String(150), nullable=False)
  cidade = db.Column(db.String(100), nullable=False)
  bairro = db.Column(db.String(100), nullable=False)
  numero = db.Column(db.String(10), nullable=False)
  telefone = db.Column(db.String(13), nullable=False)
  senha = db.Column(db.String(32), nullable=False)
  data_cad = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)

  def to_json(self):
    json_usuarios = {
      'usuarioId': self.usuarioId,
      'email': self.email,
      'nome': self.nome,
      'cpfCnpj': self.cpfCnpj,
      'cep': self.cep,
      'logradouro': self.logradouro,
      'cidade': self.cidade,
      'bairro': self.bairro,
      'numero': self.numero,
      'telefone': self.telefone
    }
    return json_usuarios

  @staticmethod
  def from_json(json_usuarios):
    email = json_usuarios.get('email')
    nome = json_usuarios.get('nome')
    cpfCnpj = json_usuarios.get('cpfCnpj')
    cep = json_usuarios.get('cep')
    logradouro = json_usuarios.get('logradouro')
    cidade = json_usuarios.get('cidade')
    bairro = json_usuarios.get('bairro')
    numero = json_usuarios.get('numero')
    telefone = json_usuarios.get('telefone')
    senha = json_usuarios.get('senha') + config.SALT
    senha_md5 = hashlib.md5(senha.encode()).hexdigest()
    return Usuario(email=email, nome=nome, cpfCnpj=cpfCnpj, cep=cep, logradouro=logradouro, cidade=cidade, bairro=bairro, numero=numero, telefone=telefone, senha=senha_md5)
