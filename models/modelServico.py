from banco import db
from datetime import datetime

class Servico(db.Model):
    __tablename__ = 'servicos'
    servicoId = db.Column(db.Integer, autoincrement=True, primary_key=True)
    dataContratacao = db.Column(db.String(300), nullable=True)
    dataConclusao = db.Column(db.String(100), nullable=True)

    clienteId = db.Column(db.Integer, db.ForeignKey(
        'clientes.clienteId'), nullable=False)

    usuarioId = db.Column(db.Integer, db.ForeignKey(
        'usuarios.usuarioId'), nullable=False)


    def to_json(self):
        json_servicos = {
            'servicoId': self.servicoId,
            'dataContratacao': self.dataContratacao,
            'dataConclusao': self.dataConclusao,
            'clienteId': self.clienteId,
            'usuarioId': self.usuarioId
        }
        return json_servicos

    @staticmethod
    def from_json(json_servicos):
        dataContratacao = json_servicos.get('dataContratacao')
        dataConclusao = json_servicos.get('dataConclusao')
        clienteId = json_servicos.get('clienteId')
        usuarioId = json_servicos.get('usuarioId')
        return Servico(dataContratacao=dataContratacao, dataConclusao=dataConclusao, clienteId=clienteId, usuarioId=usuarioId)