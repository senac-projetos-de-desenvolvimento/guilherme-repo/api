from banco import db
import hashlib
from config import config

class Login(db.Model):
    __tablename__ = 'usuarioslogins'
    id = db.Column(db.Integer, autoincrement=True, primary_key=True)
    nome = db.Column(db.String(100), nullable=False)
    email = db.Column(db.String(100), nullable=False, unique=True)
    senha = db.Column(db.String(32), nullable=False)
    tipoPermissao = db.Column(db.Integer, nullable=False)
    registroId = db.Column(db.Integer, nullable=False)

    def to_json(self):
        json_logins = {
            'id': self.id,
            'nome': self.nome,
            'email': self.email,
            'tipoPermissao': self.tipoPermissao,
            'registroId': self.registroId
        }
        return json_logins

    @staticmethod
    def from_json(json_logins):
        nome = json_logins.get('nome')
        email = json_logins.get('email')
        tipoPermissao = json_logins.get('tipoPermissao')
        registroId = json_logins.get('registroId')
        senha = json_logins.get('senha') + config.SALT
        senha_md5 = hashlib.md5(senha.encode()).hexdigest()        
        return Login(nome=nome, email=email, senha=senha_md5, tipoPermissao=tipoPermissao, registroId=registroId)
