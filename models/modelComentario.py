from banco import db
from datetime import datetime

class Comentario(db.Model):
    __tablename__ = 'comentarios'
    comentarioId = db.Column(db.Integer, autoincrement=True, primary_key=True)
    nome = db.Column(db.String(100), nullable=True)
    email = db.Column(db.String(100), nullable=True)
    comentarioPessoa = db.Column(db.String(500), nullable=True)
    dataComentario = db.Column(db.String(300), nullable=True)
    horaComentario = db.Column(db.String(100), nullable=True)

    clienteId = db.Column(db.Integer, db.ForeignKey(
        'clientes.clienteId'), nullable=False)

    usuarioId = db.Column(db.Integer, db.ForeignKey(
        'usuarios.usuarioId'), nullable=False)

    # usuario = db.relationship('Usuario')
    # clientes = db.relationship('Cliente')

    def to_json(self):
        json_comentarios = {
            'comentarioId': self.comentarioId,
            'comentarioPessoa': self.comentarioPessoa,
            'dataComentario': self.dataComentario,
            'horaComentario': self.horaComentario,
            'clienteId': self.clienteId,
            'usuarioId': self.usuarioId,
            'nome': self.nome,
            'email': self.email
        }
        return json_comentarios

    @staticmethod
    def from_json(json_comentarios):
        comentarioPessoa = json_comentarios.get('comentarioPessoa')
        dataComentario = json_comentarios.get('dataComentario')
        horaComentario = json_comentarios.get('horaComentario')
        clienteId = json_comentarios.get('clienteId')
        usuarioId = json_comentarios.get('usuarioId')
        nome = json_comentarios.get('nome')
        email = json_comentarios.get('email')
        return Comentario(comentarioPessoa=comentarioPessoa, dataComentario=dataComentario, horaComentario=horaComentario, clienteId=clienteId, usuarioId=usuarioId, nome=nome, email=email)