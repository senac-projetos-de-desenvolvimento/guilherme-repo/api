from banco import db
from datetime import datetime

class Produto(db.Model):
    __tablename__ = 'produtos'
    produtoId = db.Column(db.Integer, autoincrement=True, primary_key=True)
    produto = db.Column(db.String(100), nullable=False)
    descricao = db.Column(db.String(300), nullable=False)
    imagem = db.Column(db.String(300), nullable=True)
    imagemId = db.Column(db.String(100), nullable=True)
    valor = db.Column(db.Float, nullable=False)

    clienteId = db.Column(db.Integer, db.ForeignKey(
        'clientes.clienteId'), nullable=False)

    # imagens = db.relationship('Imagem')

    def to_json(self):
        json_produtos = {
            'produtoId': self.produtoId,
            'produto': self.produto,
            'descricao': self.descricao,
            'imagem': self.imagem,
            'imagemId': self.imagemId,
            'valor': self.valor,
            'clienteId': self.clienteId
        }
        return json_produtos

    @staticmethod
    def from_json(json_produtos):
        produto = json_produtos.get('produto')
        valor = json_produtos.get('valor')
        clienteId = json_produtos.get('clienteId')
        descricao = json_produtos.get('descricao')
        imagem = json_produtos.get('imagem')
        imagemId = json_produtos.get('imagemId')
        return Produto(produto=produto, descricao=descricao, valor=valor, clienteId=clienteId,imagem=imagem, imagemId=imagemId)