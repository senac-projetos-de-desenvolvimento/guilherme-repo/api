from flask import Flask
from flask_cors import CORS, cross_origin
from config import config
from banco import db
from blacklist import blacklist
from resources.clientes import clientes
from resources.usuarios import usuarios
from resources.projetos import projetos
from resources.produtos import produtos
from resources.imagens import imagens
from resources.comentarios import comentarios
from resources.avaliacoes import avaliacoes
from resources.servicos import servicos
from resources.acessos import acessos
from resources.logins import logins
from blacklist import blacklist
from flask_jwt_extended import JWTManager

app = Flask(__name__)
app.config.from_object(config)
db.init_app(app)
jwt = JWTManager(app)
CORS(app)

app.register_blueprint(clientes)
app.register_blueprint(usuarios)
app.register_blueprint(projetos)
app.register_blueprint(produtos)
app.register_blueprint(imagens)
app.register_blueprint(logins)
app.register_blueprint(comentarios)
app.register_blueprint(avaliacoes)
app.register_blueprint(servicos)
app.register_blueprint(acessos)

# with app.app_context():
#     api = Api(app)
#     db.init_app(app)


# @jwt.token_in_blacklist_loader
# def check_if_token_in_blacklist(decrypted_token):
#     jti = decrypted_token['jti']
#     return jti in blacklist


@app.route('/')
def raiz():
    db.create_all()
    return '<h2>ConstruObre</h2>'


if __name__ == '__main__':
    app.run(debug=True)
